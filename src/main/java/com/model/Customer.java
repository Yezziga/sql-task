package com.model;

/**
 * Class representing a customer from the database.
 */
public class Customer {
    private String customerID, firstName, lastName;

    /**
     * Constructor for creating a customer with a customer ID, first- & lastname.
     * @param customerID the customer's ID from database
     * @param firstName the customer's firstname
     * @param lastName the customer's lastname
     */
    public Customer(String customerID, String firstName, String lastName) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return  "customerID='" + customerID + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'';
    }
}
