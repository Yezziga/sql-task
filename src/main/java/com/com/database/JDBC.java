package com.com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * A singleton class that opens a connection to the database.
 */
public final class JDBC {
    private static Connection conn = null;
    private static String URL = "jdbc:sqlite::resource:Northwind_small.sqlite";

    /**
     * Private constructor.
     */
    private JDBC() {
        conn = getConnection();
    }

    /**
     * Establishes a connection to the database and returns the connection
     * @return a Connection to the database
     */
    public static Connection getConnection() {
        try {
            if(conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection(URL);
                System.out.println("Connection to SQLite has been established.");
            } else {
                System.out.println("Already connected");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
}
