package com.main;

import com.com.database.JDBC;
import com.model.Customer;

import java.sql.*;
import java.util.*;

public class Program {
    private Connection conn;

    /**
     * Constructor. Connects to the database through JDBC.
     */
    public Program() {
        conn = JDBC.getConnection();
    }

    /**
     * Queries the total number of customers, and returns a randomized value within the ID range.
     * @return a randomized value
     */
    public String existingCustomerID() {
        int id = 0;
        try {
            PreparedStatement statement = conn.prepareStatement(
                    "SELECT COUNT(customer.CustomerId) FROM customer");

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                id = resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        Random rand = new Random();
        return String.valueOf(rand.nextInt(id));
    }

    /**
     * Queries the customer's information for the given id. Creates a Character-object with the information
     * and returns it.
     * @param customerID the id of the character to fetch from database
     * @return a Character-object containing the fetched information
     */
    public Customer getCustomer(String customerID) {
        Customer customer = null;
        try {
            PreparedStatement statement = conn.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName FROM customer WHERE CustomerId =? ");
            statement.setString(1, customerID);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * Queries the names of the genres the customer with the given customer ID listens to,
     * as well as the number of times that genre occurred. The result is given in a descending order, with
     * the "most popular" genre first. Then stores the genre name and nr of occurrences in a LinkedHashMap.
     * @param customerID the customer to get genres for
     * @return a LinkedHashMap containing the genres and nr of occurrencs
     */
    public LinkedHashMap<String, Integer> getGenresForCustomer(String customerID) {
        LinkedHashMap<String, Integer> genreList = new LinkedHashMap<String, Integer>();
        PreparedStatement statement;
        try {
            statement = conn.prepareStatement(
                    "SELECT COUNT(genre.Name), genre.Name FROM customer, invoice, invoiceline, track, genre " +
                            "WHERE customer.customerId=invoice.customerId " +
                            "AND invoice.InvoiceId=invoiceline.InvoiceId " +
                            "AND invoiceline.trackId=track.trackId " +
                            "AND track.genreId=genre.genreId "+
                            "AND customer.CustomerId=? GROUP BY genre.Name ORDER BY COUNT(genre.Name) DESC");
            statement.setString(1, customerID);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                // Add genre name as key and nr of occurrences as value
                genreList.put(resultSet.getString("Name"), resultSet.getInt(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return genreList;
    }

    /**
     * Iterates through the given LinkedHashMap sets the "most popular genre" to whatever is
     * first in the list (most occurrences). Then checks if the next genre has the same nr of
     * occurrences. If not, then the customer only has 1 top genre and that is returned.
     * @param list a LinkedHashMap list of genres
     * @return a String of the top genre(s).
     */
    public String getMostPopularGenre(LinkedHashMap<String, Integer> list) {
        String topGenre ="";

        Iterator it = list.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            //System.out.println("P1: " + pair.getKey() + " = " + pair.getValue());
            topGenre = (String) pair.getKey();
            while (it.hasNext()) {
                Map.Entry pair2 = (Map.Entry)it.next();
                // If the next genre has the same value as the first, then it's a tie.
                if(pair.getKey()!=pair2.getKey() && pair.getValue() == pair2.getValue()) {
                    //System.out.println("P2 " + pair2.getKey() + " = " + pair2.getValue());
                    topGenre += ", " + pair2.getKey();
                } else {
                    continue;
                }
            }
        }
        return topGenre;
    }

    /**
     * Prompts the user for input
     * @return the user's input
     */
    public String promptForInput() {
        System.out.println("Enter a customer ID ");
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }


    public static void main(String[] args) {
        Program p = new Program();
        String input = p.promptForInput();
        Customer customer;
        // user did not input an ID (numbers)
        if(!input.matches("[0-9]+")) {
            input = p.existingCustomerID();
            System.out.println("No valid ID given. Randomizing ID: " + input);
        }
        customer = p.getCustomer(input);
        if(customer == null) {
            System.out.println("No customer with that ID exists. Please try again.");
        } else {
            System.out.println(customer.toString());
            LinkedHashMap<String, Integer> list = p.getGenresForCustomer(input);
            System.out.println("Most popular genre(s): " + p.getMostPopularGenre(list));
        }


    }
}
